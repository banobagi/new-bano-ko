//남성모발이식 모션
$('.hair-man1__list').slick({
    autoplay: true,
    slidesToShow:1,
	pauseOnHover:false,
    slidesToScroll: 1,
    infinite: true,
	prevArrow:'.js-hair-prev',
	nextArrow:'.js-hair-next',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
				autoplay: false,
				slidesToShow:1,
			    slidesToScroll: 1
            }
        }
    ]
});

$('.hair-man1__list').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	var hairThumbnaiil = $('.hair-thumbnail1__item');
	hairThumbnaiil.eq(nextSlide).addClass('is-active').siblings().removeClass('is-active');
});

$('.js-thumbnail').click(function(e){
  e.preventDefault();
  var slideno = $(this).data('slide');
  $('.hair-man1__list').slick('slickGoTo',slideno);
});

$('.js-care').on('click',function(e){
	e.preventDefault();
	$(this).parents('.care-style1__item').addClass('is-active').siblings().removeClass('is-active');
	clearInterval(careTimer);
	careTimer = setInterval(hairMotion2, 2000);
});
function hairMotion2() {
	var careItem = $('.care-style1__item');
	var careNum = $('.care-style1__item.is-active').index();
	if(careNum == 3){
		careNum = -1;
	}
	careItem.eq(careNum).removeClass('is-active');
	careItem.eq(careNum+1).addClass('is-active');
}
careTimer = setInterval(hairMotion2, 2000);

//남성모발이식 모션
var centerSlide = $('.js-center-slide').slick({
	centerMode: true,
	pauseOnHover:false,
    slidesToShow:1,
    slidesToScroll: 1,
	arrow:false,
	centerPadding: '230px',
	prevArrow:'.js-center-prev',
	nextArrow:'.js-center-next',
	adaptiveHeight: true,
	autoplay: true,
    responsive: [
        {
            breakpoint: 1180,
            settings: {
				autoplay: false,
				centerMode: false,
				centerPadding: '0px',
				slidesToShow:1,
			    slidesToScroll: 1
            }
        }
    ]
});

centerSlide.on('beforeChange', function(event, slick, currentSlide, nextSlide){
	var currentNum = $('.slide-style1__item.slick-active').index();
	$('.slide-style1__item').eq(nextSlide+1).addClass('is-prev').siblings().removeClass('is-prev');
	$('.slide-style1__item').eq(nextSlide+3).addClass('is-next').siblings().removeClass('is-next');
});
