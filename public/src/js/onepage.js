$(document).ready(function(){
	$(window).on("load resize", function () {
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);
		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		function disableScrollify(toggle){
		    if(toggle){
				$('body').css('overflow','auto');
		        $.scrollify.disable();
		    } else {
				$.scrollify.enable();
		        $.scrollify(settings);
		    }
		}
		$.scrollify(settings);
		var firstSection = $('.detail-section').eq(0).attr('data-section-name');
		//상단으로 가기
		$('.scroll-top').on('click', function(e) {
			if(firstSection == 'hair-section99'){
				e.preventDefault();
				$.scrollify.move("#hair-section99");
				videoPlay();
			}
		});

		if ($('body').attr('only-mobile') == 'true'){
			var detail = $('.onepage').attr('class');
			var detailIs = detail.split(' ');
			if(detailIs[1] =='type-detail'){
				disableScrollify(1);
				var winH = $(window).height();
				$('.detail-section.type-visual').outerHeight(winH);
			}
			if(firstSection == 'hair-section99'){
				videoReset();
			}
		} else {
			disableScrollify(0);
			if(firstSection == 'hair-section99'){
				playCheck();
			}
		}
		if ($('body').attr('data-mobile') == 'true'){
			if(firstSection == 'hair-section99'){
				videoStop();
				// onepage_player.setLoop(true);
			}
		}else{
			if(firstSection == 'hair-section99'){
				videoPlay();
				// onepage_player.setLoop(false);
			}
		}

		//상담팝업
		$('.js-consultation').click(function(e) {
			if ($('body').attr('data-mobile') == 'false'){
				 $.scrollify.disable();
			 }
		});
		$('.popup-c__close').click(function(e) {
			if ($('body').attr('data-mobile') == 'false'){
				  $.scrollify.enable();
			 }
		});
	});

	var settings = {
		section : ".detail-section",
		interstitialSection:".footer",
		scrollbars:false,
		scrollSpeed: 700,
    	updateHash: true,
		before:function(i,panels) {
			//로딩전
			var ref = panels[i].attr("data-section-name");
			$(".fs-nav .is-active").removeClass("is-active");
  			$(".fs-nav").find("a[href=\"#" + ref + "\"]").addClass("is-active");
			if(ref == 'hair-section99'){
				videoPlay();
			}else{
				videoStop();
			}
		},
		afterRender:function() {
			var pagination = "<ul class=\"fs-nav\">";
			var activeClass = "";
			$(".detail-section").each(function(i) {
				activeClass = "";
				if(i===0) {
				  activeClass = "is-active";
				}
				pagination += "<li class='fs-nav__item'><a class=\"" + activeClass + " fs-nav__link\" href=\"#" + $(this).attr("data-section-name") + "\"></a></li>";
			});
			pagination += "</ul>";
			$(".onepage").append(pagination);
			$(".fs-nav a").on("click",$.scrollify.move);
		}
	};
	var onepage_iframe = $(".onepage-video__iframe");
	var onepage_player = new Vimeo.Player(onepage_iframe);

	function playCheck(){
		onepage_player.on("ended", function() {
			console.log("ended!");
			$.scrollify.move("#home");
			// $.scrollify.next();
	    });
	}

	//영상플레이
	function videoPlay() {
		onepage_player.play();
	    // f = $(".onepage-video__iframe");//<iframe src="http://player.vimeo.com/video/12345678" id="vimeo_iframe"></iframe>";
		//
	    // if (!f || !f.attr("src")) return;
		//
	    // url = f.attr("src").split("?")[0];
		//
	    // var data = {
	    //     "method" : "play",
	    //     "value" : 1
	    // }
	    // f[0].contentWindow.postMessage(JSON.stringify(data), url);
	}
	//영상 일시정지
	function videoStop() {
		if(onepage_player){
			onepage_player.pause();
		}
		
	    // f = $(".onepage-video__iframe");//<iframe src="http://player.vimeo.com/video/12345678" id="vimeo_iframe"></iframe>";
	    // if (!f || !f.attr("src")) return;
		//
	    // url = f.attr("src").split("?")[0];
		//
	    // var data = {
	    //     "method" : "pause",
	    //     "value" : 1
	    // }
	    // f[0].contentWindow.postMessage(JSON.stringify(data), url);
	}
	function videoReset(){
		onepage_player.setCurrentTime(0);
		onepage_player.pause();
	}


});

//메뉴오픈시 디테일 스크롤 움직임 막기
$('.mobile-menu').on('click',function(e){
	if ($('body').attr('data-mobile') == 'true' && $('body').attr('only-mobile') == 'false'){
		if($('.header').hasClass('js-open-m')){
			$.scrollify.enable();
		}else{
			$.scrollify.disable();
		}
	}
});
