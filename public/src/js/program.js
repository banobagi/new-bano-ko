$(document).ready(function(){
	$('.js-top').click(function(e){
		e.preventDefault();
		$('html,body').animate({'scrollTop':'0'},300);
	});
	$('.visual__list').cycle({
		slides:'.visual__item',
		fx:'scrollHorz',
		loop:false,
		timeout:4000,
		swipe:true,
		pager:'.visual__pager',
		pagerTemplate:'<span class="visual__bull">&bull;</span>',
		pagerActiveClass:'is-active'
	});

	$('.longweek__btn').on('click',function(e){
		e.preventDefault();
		var longElem = $(this).closest('.longweek__box');
		var bottomElem = $(this).closest('.longweek__box').find('.longweek__bottom');
		if(longElem.hasClass('is-active')){
			longElem.removeClass('is-active');
			bottomElem.slideUp(300);
			$(this).text('내용 확인하기')
		}else{
			longElem.addClass('is-active');
			bottomElem.slideDown(300);
			$(this).text('닫기')
		}
	});

	//vlifting
	$(window).on("load resize", function () {
		if ($('body').attr('data-mobile') == 'false'){
			try {
				clearInterval(playbfaf);
			} catch(error) {}
			$('.vlifting-1909__item').removeClass('is-active');
			$('.vlifting-1909__item').on('mouseenter',function(){
				$(this).addClass('is-active');
				$(this).find('.vlifting-1909__info').text('After 3M');
			}).on('mouseleave',function(){
				$(this).removeClass('is-active');
				$(this).find('.vlifting-1909__info').text('Before');
			});
		}else{
			var cnt = 0;
			$('.vlifting-1909__item').off('mouseenter mouseleave');
			playbfaf = setInterval(function() {
				if(cnt == 1){
					$('.vlifting-1909__item').removeClass('is-active');
					$('.vlifting-1909__info').text('Before');
					cnt = 0;
				}else{
					$('.vlifting-1909__item').addClass('is-active');
					$('.vlifting-1909__info').text('After 3M');
					cnt = 1;
				}
			}, 2000);
		}
	});


	// before&after slider
	$(".js-before-slide").slick({
		slidesToShow: 1,
    	slidesToScroll: 1,
		autoplay: true,
		infinite: true,
		fade: true,
		autoplaySpeed: 3000,
		cssEase: "linear",
		prevArrow: ".js-before-prev",
		nextArrow: ".js-before-next",
	});

	$(".js-thumbnail-slide").slick({
	    slidesToShow: 3,
		slidesToScroll: 3,
		cssEase: "linear",
		arrows: false,
		infinite: false,
		dots: true,
		swipe: false,
		customPaging: function(slider, i) {
			var thumb = $(slider.$slides[i]).data();

			return '<a class="program-before__link" data-dot="' + i + '">'+ (i+1) +'</a>';
		},
		dotsClass: "program-before__dots"
	});
	var currentNum;
	$(".js-before-slide").off("beforeChange").on("beforeChange", function(event, slick, currentSlide, nextSlide){
		$(".program-before__item2").eq(nextSlide).addClass("is-active").siblings().removeClass("is-active");
		$(".js-thumbnail-slide").slick("slickGoTo", nextSlide);

		var beforeNum = $(".program-before__item").eq(nextSlide).attr("data-page");
		var dotNum = $(this).parents('.program-before__outer').find('.program-before__link.is-active').attr("data-dot");

		// console.log('before'+currentNum)
		// console.log('after'+currentSlide)
		// console.log('beforeNum : ' + beforeNum)

		if(beforeNum%3 == 0){
			if(beforeNum == 0){
				$(".program-before__dots li").eq(0).trigger("click");
			}

			if(currentNum > currentSlide){
				$(".program-before__dots li").eq(Math.floor(beforeNum/3)).trigger("click");
			}else{
				$('.program-before__link').removeClass('is-active')
				$(".program-before__dots li").eq(Math.floor(beforeNum/3)).find(".program-before__link").addClass('is-active');
			}

		}else if(beforeNum%3 == 2){
			if(currentNum > currentSlide){
				$('.program-before__link').removeClass('is-active')
				$(".program-before__dots li").eq(dotNum-1).find(".program-before__link").addClass('is-active');
			}else{
				$(".program-before__dots li").eq(Math.floor(beforeNum/3)).trigger("click");
			}
		}

		// return currentNum = $(".program-before__item").eq(currentSlide).attr("data-page");
	});

	$(".js-before-slide").off("afterChange").on('afterChange', function(event, slick, currentSlide){
		/*var beforeNum = $(".program-before__item").eq(currentSlide).attr("data-page");
		var dotNum = $(this).parents('.program-before__outer').find('.program-before__link.is-active').attr("data-dot");

		// console.log('before'+currentNum)
		// console.log('after'+currentSlide)
		// console.log('beforeNum : ' + beforeNum)

		if(beforeNum%3 == 0){
			if(beforeNum == 0){
				$(".program-before__dots li").eq(0).trigger("click");
			}

			if(currentNum < currentSlide){
				$(".program-before__dots li").eq(Math.floor(beforeNum/3)).trigger("click");
			}
		}else if(beforeNum%3 == 2){
			if(currentNum > currentSlide){
				$('.program-before__link').removeClass('is-active')
				$(".program-before__dots li").eq(dotNum-1).find(".program-before__link").addClass('is-active');
			}else{
				$(".program-before__dots li").eq(Math.floor(beforeNum/3)).trigger("click");
			}
		}*/
	});

	// thum slide click event
	$(".program-before__item2").on("click", function(){
		var thumbnailNum = $(this).attr('data-page');

		$(".js-before-slide").slick("slickGoTo", thumbnailNum);
	});

	// thum dot click event
	$(".program-before__dots li").eq(0).find(".program-before__link").addClass("is-active");
	$(".program-before__dots li").on("click", function(){
		var dotNum = $(this).find('.program-before__link').attr("data-dot");

		$(".program-before__link").removeClass("is-active");
		$(this).find(".program-before__link").addClass("is-active")
		$(".js-before-slide").slick("slickGoTo", dotNum*3);
		// console.log(dotNum*3)
	});

	// banobagi tv
	$(".program-tv__item").eq(0).addClass("is-active"); // default
	$(".program-tv__link").on("click", function(e){
		e.preventDefault();

		var banotvItem = $(this).parents(".program-tv__item");
		var videoId = $(this).attr("data-ytb-id");

		banotvItem.addClass("is-active").siblings().removeClass("is-active");

		// change youtube videoId
		banoPlayer.stopVideo();
		banoPlayer.loadVideoById(videoId);

		moveBanoTv();
	});
});

/*
	banoTV Youtube API 로드
*/
var banoTag = document.createElement('script');
banoTag.src = "https://www.youtube.com/iframe_api";
var banoFirstScriptTag = document.getElementsByTagName('script')[0];
banoFirstScriptTag.parentNode.insertBefore(banoTag, banoFirstScriptTag);

var banoPlayer;
var banoVideoId = $(".program-tv__item").eq(0).find(".program-tv__link").attr("data-ytb-id");
// console.log(banoVideoId);

function onYouTubeIframeAPIReady() {
	banoPlayer = new YT.Player('program-iframe', {
		height: '360',
		width: '640',
		videoId: banoVideoId
	});
}

function moveBanoTv(){
	// focus
	var banoTop = $(".program-tv").offset().top;
	var headerH = $(".header").height();

	var moveTop = banoTop - headerH;

	$('html, body').stop().animate({
		scrollTop: moveTop
	}, 500);
}
