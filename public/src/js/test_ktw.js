$(document).ready(function(){

     scroll_button();
    function scroll_button(){
        var $button = $('.scroll-btn');

        if(!$button.length) { return; }
        var scrollBtnW = $button.width();
        // circle progress scroll
        $.circleProgress.defaults.animation = false;
        $.circleProgress.defaults.value = 0;
        $.circleProgress.defaults.size = scrollBtnW;
        $.circleProgress.defaults.startAngle = -Math.PI / 4 * 2;
        $.circleProgress.defaults.thickness = '4';
        $.circleProgress.defaults.emptyFill = 'rgba(221, 221, 221, 0.9)';
        $.circleProgress.defaults.fill = { color: '#b916a4' };


        $('.scroll-btn__progress').circleProgress();

        $(window).on('load resize',function(){
    	    scroll_button_resize();
    	});
        $button.on('click', function(e) {
    		e.preventDefault();
    		$('html, body').stop().animate({
    			scrollTop: 0
    		}, 500);
        });
    }

    function scroll_button_resize(){
        $('.scroll-btn__progress').circleProgress('redraw');
    }

    $.progressIndicator({
        direction : 'top',
        barColor: 'rgb(255, 132, 0)',
        percentageEnabled : false,
        percentageColor: '#222',
        easingSpeed : 0.5,
        height: 5,
        target : 'body', // selector
        onStart : function(){
            console.log("onStart");
        },
        onEnd : function(){
            console.log("onEnd");
        },
        onProgress : function(perecent){
            console.log(perecent);
        }
    });
});

function dragEnter(ev) {
	ev.preventDefault();
}

function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	ev.target.appendChild(document.getElementById(data));
    //var box = $('#monalisa1111').parents('.test-box').attr('data-num')
}
