$(document).ready(function(){
	//양악집중케어 (남성모발이식이랑 소스 조금 다름)
	(function(){
		var pageState = 'false';
		var laserState = 'false';
		var pageElem = document.querySelector('.page-smart .care-style1');
		var laserElem = document.querySelector('.page-laser .care-style1');
		function pageMotion() {
			var pageItem = $('.page-smart .care-style1__item');
			var pageNum = $('.page-smart .care-style1__item.is-active').index();
			if(pageNum == 3){
				pageNum = -1;
			}else if(pageNum == 2){
				$('.page-smart .care-style1__result, .page-smart .care-style1__arrow').addClass('is-active');
			}
			pageItem.eq(pageNum).removeClass('is-active');
			pageItem.eq(pageNum+1).addClass('is-active');

		}
		function laserMotion() {
			var laserItem = $('.page-laser .care-style1__item');
			var laserNum = $('.page-laser .care-style1__item.is-active').index();
			if(laserNum == 3){
				laserNum = -1;
			}else if(laserNum == 2){
				$('.page-laser .care-style1__result, .page-laser .care-style1__arrow').addClass('is-active');
			}
			laserItem.eq(laserNum).removeClass('is-active');
			laserItem.eq(laserNum+1).addClass('is-active');
		}
		function showPage(){
			var posY = pageElem.getBoundingClientRect().top;
			if(posY < 800){
				if(pageState == 'true') return false;
				pageTimer = setInterval(pageMotion, 2000);
				pageState = 'true';
			}
		}
		function showLaser(){
			var laserY = laserElem.getBoundingClientRect().top;
			if(laserY < 800){
				if(laserState == 'true') return false;
				laserTimer = setInterval(laserMotion, 2000);
				laserState = 'true';
			}else{
			}
		}
		$(window).on('scroll resize load',function(){
			var tabActive = $('.new-tab__item.is-active').index();
			if(tabActive == 0){
				try {showPage();} catch(error) {}
			}else if(tabActive == 1){
				try {showLaser();} catch(error) {}

			}
		});
		$('.js-care').on('click',function(e){
			e.preventDefault();
			$(this).parents('.care-style1__item').addClass('is-active').siblings().removeClass('is-active');
			var itemNum = $(this).parents('.care-style1__item').index();
			if(itemNum == 3){
				$(this).parents('.care-style1').find('.care-style1__result, .care-style1__arrow').addClass('is-active');
			}
			var tabActive = $('.new-tab__item.is-active').index();
			if(tabActive == 0){
				clearInterval(pageTimer);
				pageTimer = setInterval(pageMotion, 2000);
			}else if(tabActive == 1){
				clearInterval(laserTimer);
				laserTimer = setInterval(laserMotion, 2000);
			}
		});
		$('.js-inner-content').on('click',function(e){
			var tabActive = $('.new-tab__item.is-active').index();
			if(tabActive == 0){
				try {clearInterval(laserTimer); laserState = 'false';} catch(error) {}
			}else if(tabActive == 1){
				try { clearInterval(pageTimer); pageState = 'false';} catch(error) {}
			}
		});
	})();

});
