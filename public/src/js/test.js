$(document).ready(function(){
	// fakescroll.preset('scroll-presetpv-1', 'ios-mode');
	fakescroll.preset('scroll-presetpv-2-1', 'slider-x', { barsize:16, min:1, max:13, step:1, value:1, onchange:function(value){
	    //document.getElementById('sliderpv-x-value').innerHTML='x value : '+value;
		$('.box.nth-1 .img-box').css({'background':'url("../../images/test/1/'+(value-1)+'.jpg") no-repeat center center / cover'});
		$('.box.nth-1 .img-box__inner').css({'background':'url("../../images/test/1/'+value+'.jpg") no-repeat center center / cover'});
	}});
	fakescroll.preset('scroll-presetpv-2-2', 'slider-x', { barsize:16, min:1, max:13, step:1, value:1, onchange:function(value){
	    //document.getElementById('sliderpv-x-value').innerHTML='x value : '+value;
		$('.box.nth-2 .img-box').css({'background':'url("../../images/test/2/'+(value-1)+'.jpg") no-repeat center center / cover'});
		$('.box.nth-2 .img-box__inner').css({'background':'url("../../images/test/2/'+value+'.jpg") no-repeat center center / cover'});
	}});
});
